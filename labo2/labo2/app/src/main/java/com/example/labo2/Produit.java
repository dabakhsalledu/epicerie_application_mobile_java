package com.example.labo2;

public class Produit {
    private int id;
    private int img;
    private String cat;
    private Double prix;
    private String descy;
    private int quantite=1;

    public Produit(int id,int img,String cat, double prix, String descy){
        this.id=id;
        this.img=img;
        this.cat=cat;
        this.prix=prix;
        this.descy=descy;
    }

    public int getId() {
        return id;
    }

    public String getCat() {
        return cat;
    }

    public String getDescy() {
        return descy;
    }

    public int getImg() {
        return img;
    }

    public Double getPrix() {
        return prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void updatequantiteplus(){
        this.quantite++;
    }
    public void updatequantitemoins(){
        if(this.quantite>1){
            this.quantite--;
        }
    }
    public double totalPrix(){
        return this.prix * this.quantite;
    }
}

