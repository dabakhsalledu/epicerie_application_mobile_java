package com.example.labo2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class panierArrayAdapter extends ArrayAdapter<Produit> {

    private Context ctx;
    private int res;
    private ArrayList<Produit> produitArrayList;
    private Button btnPlus;
    private Button btnMoins;

    public panierArrayAdapter(@NonNull Context context, int resource, @NonNull List<Produit> objects) {
        super(context, resource, objects);

        this.ctx=context;
        this.res=resource;

        this.produitArrayList=new ArrayList<Produit>();
        this.produitArrayList = (ArrayList<Produit>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //1
        Produit panierTemp = this.produitArrayList.get(position);

        //2
        LayoutInflater inflater = LayoutInflater.from(this.ctx);
        convertView=inflater.inflate(this.res,parent,false);
        //3
        TextView lblPrix=(TextView) convertView.findViewById(R.id.idPrixPanier);
        ImageView image = (ImageView) convertView.findViewById(R.id.imagId);
        //4
        lblPrix.setText(String.valueOf(panierTemp.getPrix()));
        TextView quantite=(TextView) convertView.findViewById(R.id.qtsProduit);
        TextView prixTotal=(TextView) convertView.findViewById(R.id.idPrixTotal);
        Button btnPlus=(Button) convertView.findViewById(R.id.btnplus);
        Button btnMoins=(Button) convertView.findViewById(R.id.btnmoins);
        if(panierTemp.getImg()==700012){
            image.setImageResource(R.drawable.brocoli);
        }
        if(panierTemp.getImg()==700009){
            image.setImageResource(R.drawable.pomme);
        }
        if(panierTemp.getImg()==700008){
            image.setImageResource(R.drawable.mangue);
        }
        if(panierTemp.getImg()==700005){
            image.setImageResource(R.drawable.chou);
        }
        if(panierTemp.getImg()==700002){
            image.setImageResource(R.drawable.concombre);
        }
        if(panierTemp.getImg()==700011){
            image.setImageResource(R.drawable.raisin);
        }
        if(panierTemp.getImg()==700003){
            image.setImageResource(R.drawable.carotte);
        }
        btnPlus = (Button)convertView.findViewById(R.id.btnplus);
        btnMoins=(Button)convertView.findViewById(R.id.btnmoins);
        quantite.setText(String.valueOf(panierTemp.getQuantite()));
        //prixTotal.setText(String.valueOf(panierTemp.getPrix()));
        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                panierTemp.updatequantiteplus();
                quantite.setText(String.valueOf(panierTemp.getQuantite()));
            }
        });
        btnMoins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                panierTemp.updatequantitemoins();
                quantite.setText(String.valueOf(panierTemp.getQuantite()));
            }
        });


        return convertView;
    }
}
