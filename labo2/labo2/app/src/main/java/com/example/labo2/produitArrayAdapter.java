package com.example.labo2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class produitArrayAdapter extends ArrayAdapter<Produit> {

    private Context ctx;
    private int res;
    private static ArrayList<Produit> produitArrayList;


    public produitArrayAdapter(@NonNull Context context, int resource, @NonNull List<Produit> objects) {
        super(context, resource, objects);

        this.ctx=context;
        this.res=resource;
        this.produitArrayList = new ArrayList<Produit>();
        this.produitArrayList = (ArrayList<Produit>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //1 recupere un objet dans mon arraylist
        Produit produitTemp=this.produitArrayList.get(position);
        //2 prepare la vue Produit.xml a recevoir les infos de ChocoTemp
        LayoutInflater inflater = LayoutInflater.from(this.ctx);
        convertView=inflater.inflate(this.res,parent,false);
        //3-Obtenir des references vers les views qui sont dans convertView/choco.xml
        ImageView imageView = (ImageView)convertView.findViewById(R.id.imgId);
        TextView prix = (TextView)convertView.findViewById(R.id.prixId);
        TextView desc = (TextView)convertView.findViewById(R.id.descId);
        //Pour le bouton ADD
        Button btnADD = (Button)convertView.findViewById(R.id.btnAjout);
        btnADD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Panier.arl.contains(produitTemp)==false){
                    Panier.arl.add(produitTemp);
                }else{
                    produitTemp.updatequantiteplus();
                }


            }
        });

        //4-utiliser les references pour remplir les views avec les infos de chocoTemp
        prix.setText(String.valueOf(produitTemp.getPrix()));
        desc.setText(produitTemp.getDescy());
        //imageView.setImageResource(produitTemp.getImg());
        if(produitTemp.getImg()==700010){
            imageView.setImageResource(R.drawable.banane);
        }
        if(produitTemp.getImg()==700003){
            imageView.setImageResource(R.drawable.carotte);
        }
        if(produitTemp.getImg()==700005){
            imageView.setImageResource(R.drawable.chou);
        }
        if(produitTemp.getImg()==700008){
            imageView.setImageResource(R.drawable.mangue);
        }

        if(produitTemp.getImg()==700010){
            imageView.setImageResource(R.drawable.banane);
        }
        if(produitTemp.getImg()==700011){
            imageView.setImageResource(R.drawable.raisin);
        }
        if(produitTemp.getImg()==700012){
            imageView.setImageResource(R.drawable.brocoli);
        }

        //5-Retourne le produit.xml rempli vers le listView
        return convertView;
    }
}
