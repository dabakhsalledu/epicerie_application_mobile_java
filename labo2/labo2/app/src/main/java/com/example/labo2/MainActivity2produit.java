package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity2produit extends AppCompatActivity {
    private ListView lst;
    public static TextView entete;
   // private ProduitArrayAdapter adapter;
    private produitArrayAdapter adapter1;
    private Button btnCards;
    private produitTableDataGatWay myDb;
    private ArrayList<Produit> ProduitArrayList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity2produit);
        this.btnCards=(Button) findViewById(R.id.btnCard);
        this.btnCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),MainActivity3.class);
                startActivity(i);
            }
        });



        this.lst=(ListView) findViewById(R.id.lstProduit);
        entete=(TextView) findViewById(R.id.entete_activity);

        String name= getIntent().getStringExtra("cat1");
        entete.setText(name);
        String name1= getIntent().getStringExtra("cat2");
        entete.setText(name1);
        String name2= getIntent().getStringExtra("cat3");
        entete.setText(name2);
        String name3= getIntent().getStringExtra("cat4");
        entete.setText(name3);
        String name4= getIntent().getStringExtra("cat5");
        entete.setText(name4);
        String name5= getIntent().getStringExtra("cat6");
        entete.setText(name5);

        this.myDb = new produitTableDataGatWay(this,
                "ssaa",null,1);
        this.myDb.Open();
        this.ProduitArrayList= new ArrayList<Produit>();
        this.ProduitArrayList = this.myDb.SelectAllProduits();
        if(this.ProduitArrayList.size()<1)
        {
            this.myDb.SeedFruitsDataBase();
            this.ProduitArrayList = this.myDb.SelectAllProduits();
        }
        this.adapter1 = new produitArrayAdapter(this,R.layout.produit,ProduitArrayList);
        this.lst.setAdapter(adapter1);




    }



}